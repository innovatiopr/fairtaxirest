package com.innovatio.fairtaxi.bean;


/**
 * Created by miguelmal on 10/6/18.
 */
public class RecommendationRequest {
    RecommendationType type;
    LngLat location;

    public RecommendationType getType() {
        return type;
    }

    public void setType(RecommendationType type) {
        this.type = type;
    }

    public LngLat getLocation() {
        return location;
    }

    public void setLocation(LngLat location) {
        this.location = location;
    }
}
