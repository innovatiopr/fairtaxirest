package com.innovatio.fairtaxi.dao;

import com.innovatio.fairtaxi.bean.Fee;
import com.innovatio.fairtaxi.bean.LngLat;
import org.springframework.jdbc.core.SingleColumnRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class ZoneDao extends NamedParameterJdbcDaoSupport {

    public ZoneDao(DataSource dataSource) {
        setDataSource(dataSource);
    }

    public Boolean isAirport(LngLat location) {
        String zone = getZone(location);
        return "Zona 1".equals(zone);
    }

    public String getZone(LngLat location) {
        String sql = "with location as ( " +
                "      select " +
                "         st_setsrid(st_point(:lng, :lat), 4326) as location " +
                "     ) " +
                " select " +
                "   z.zona" +
                "   from location cross join taxi_zones z " +
                " where st_within(location.location, z.geom) " +
                " order by st_area(z.geom) ";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("lng", location.getLng());
        params.addValue("lat", location.getLat());
        final List<String> results = getNamedParameterJdbcTemplate().query(sql, params, new SingleColumnRowMapper<>(String.class));
        return !results.isEmpty() ? results.get(0) : null;
    }

    public Fee getZoneFees(LngLat origin, LngLat dest) {
        String sql = "with trip as ( " +
                "      select " +
                "         st_setsrid(st_point(:orig_lng, :orig_lat), 4326) orig, " +
                "         st_setsrid(st_point(:dest_lng, :dest_lat), 4326) dest " +
                "     ) " +
                " select " +
                "   orig_zone.zona orig_zone, " +
                "   orig_zone.descripcion orig_desc, " +
                "   dest_zone.zona dest_zone, " +
                "   dest_zone.descripcion dest_desc, " +
                "   tarifa " +
                " from trip " +
                "        cross join taxi_zones orig_zone " +
                " join tarifas_por_zona on orig_zone.zona = tarifas_por_zona.origen " +
                " join taxi_zones dest_zone on dest_zone.zona = tarifas_por_zona.destino " +
                " where st_within(trip.orig, orig_zone.geom) " +
                " and st_within(trip.dest, dest_zone.geom) " +
                " order by st_area(orig_zone.geom), st_area(dest_zone.geom)";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("orig_lng", origin.getLng());
        params.addValue("orig_lat", origin.getLat());
        params.addValue("dest_lng", dest.getLng());
        params.addValue("dest_lat", dest.getLat());
        final List<Fee> results = getNamedParameterJdbcTemplate().query(sql, params, (rs, i) -> {
            Fee f = new Fee();
            f.setName(String.format("%s to %s",
                    rs.getString("orig_zone"),
                    rs.getString("dest_zone")
            ));
            f.setDesc(String.format("%s to %s",
                    rs.getString("orig_desc"),
                    rs.getString("dest_desc")
            ));
            f.setFee(rs.getBigDecimal("tarifa"));
            return f;
        });

        return !results.isEmpty() ? results.get(0) : null;
    }


}
