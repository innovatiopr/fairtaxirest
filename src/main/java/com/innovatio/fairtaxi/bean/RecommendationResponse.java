package com.innovatio.fairtaxi.bean;

import java.util.List;

/**
 * Created by miguelmal on 10/6/18.
 */
public class RecommendationResponse {
    List<ViewPrInfo> recommendations;

    public List<ViewPrInfo> getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(List<ViewPrInfo> recommendations) {
        this.recommendations = recommendations;
    }
}
