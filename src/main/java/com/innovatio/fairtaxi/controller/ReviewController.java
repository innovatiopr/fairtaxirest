package com.innovatio.fairtaxi.controller;

import com.innovatio.fairtaxi.bean.RecommendationRequest;
import com.innovatio.fairtaxi.bean.RecommendationResponse;
import com.innovatio.fairtaxi.bean.TripReview;
import com.innovatio.fairtaxi.service.RecommendationService;
import com.innovatio.fairtaxi.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * Created by miguelmal on 10/6/18.
 */

@RestController
public class ReviewController {

    @Autowired
    ReviewService reviewService;

    @PostMapping("/tripReview")
    @ResponseStatus(value = HttpStatus.OK)
    void addReview(@RequestBody TripReview tripReview) {
        reviewService.addReview(tripReview);
    }

}
