package com.innovatio.fairtaxi.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class FareEstimateResponse {

    private List<Fee> fees = new ArrayList<>();
    private BigDecimal total;
    private BigDecimal suggestedTip;
    private BigDecimal totalWithTip;
    private TripFeeType tripFeeType;

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getSuggestedTip() {
        return suggestedTip;
    }

    public void setSuggestedTip(BigDecimal suggestedTip) {
        this.suggestedTip = suggestedTip;
    }

    public List<Fee> getFees() {
        return fees;
    }

    public void setFees(List<Fee> fees) {
        this.fees = fees;
    }

    public BigDecimal getTotalWithTip() {
        return totalWithTip;
    }

    public void setTotalWithTip(BigDecimal totalWithTip) {
        this.totalWithTip = totalWithTip;
    }

    public TripFeeType getTripFeeType() {
        return tripFeeType;
    }

    public void setTripFeeType(TripFeeType tripFeeType) {
        this.tripFeeType = tripFeeType;
    }
}
