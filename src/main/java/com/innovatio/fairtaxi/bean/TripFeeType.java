package com.innovatio.fairtaxi.bean;

public enum TripFeeType {

    METER,
    ZONE
}
