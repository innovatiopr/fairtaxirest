package com.innovatio.fairtaxi.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ TYPE })
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = { FareEstimateRequestValidator.class })
public @interface ValidFareEstimateRequest {
    String message() default "Must provide either idleTimeSeconds or totalTripTimeSeconds";
    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default { };
}
