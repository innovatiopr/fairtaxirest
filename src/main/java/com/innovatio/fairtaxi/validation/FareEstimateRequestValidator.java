package com.innovatio.fairtaxi.validation;

import com.innovatio.fairtaxi.bean.FareEstimateRequest;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FareEstimateRequestValidator implements ConstraintValidator<ValidFareEstimateRequest, FareEstimateRequest> {


    @Override
    public void initialize(ValidFareEstimateRequest req) {
    }

    @Override
    public boolean isValid(FareEstimateRequest req, ConstraintValidatorContext constraintValidatorContext) {
        return req.getTotalTripTimeSeconds() != null || req.getIdleTimeSeconds() != null;
    }

}