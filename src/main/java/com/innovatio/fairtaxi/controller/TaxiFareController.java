package com.innovatio.fairtaxi.controller;


import com.innovatio.fairtaxi.bean.FareEstimateRequest;
import com.innovatio.fairtaxi.bean.FareEstimateResponse;
import com.innovatio.fairtaxi.bean.LngLat;
import com.innovatio.fairtaxi.bean.ServiceType;
import com.innovatio.fairtaxi.service.FareEstimateService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
public class TaxiFareController {

    private FareEstimateService fareEstimateService;

    public TaxiFareController(FareEstimateService fareEstimateService) {
        this.fareEstimateService = fareEstimateService;
    }

    @PostMapping("/fare-estimate")
    public FareEstimateResponse estimateFare(@RequestBody @Valid FareEstimateRequest fareReq) {
        return fareEstimateService.estimateFare(fareReq);
    }

    @PostMapping("/default-service-type")
    public Map<String, ?> getDefaultServiceType(@RequestBody @Valid LngLat location) {
        final ServiceType serviceType = fareEstimateService.getDefaultServiceType(location);
        Map<String, Object> resp = new HashMap<>();
        resp.put("serviceType", serviceType);
        return resp;
    }

}
