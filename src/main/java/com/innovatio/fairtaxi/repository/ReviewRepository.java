package com.innovatio.fairtaxi.repository;

import com.innovatio.fairtaxi.entity.Review;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by miguelmal on 10/6/18.
 */
public interface ReviewRepository extends CrudRepository<Review, Long> {
}
