package com.innovatio.fairtaxi.bean;

import java.math.BigDecimal;

public class Fee {

    private String name;
    private String desc;
    private BigDecimal fee;

    public Fee() {
    }

    public Fee(String name, String desc, BigDecimal fee) {
        this.name = name;
        this.desc = desc;
        this.fee = fee;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }
}
