package com.innovatio.fairtaxi.bean;

public enum ServiceType {

    TAXI,
    TAXI_TURISTICO
}
