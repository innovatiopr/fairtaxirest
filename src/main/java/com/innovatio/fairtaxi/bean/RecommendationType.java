package com.innovatio.fairtaxi.bean;

/**
 * Created by miguelmal on 10/6/18.
 */
public enum RecommendationType {
    RESTAURANT,
    ATTRACTION
}
