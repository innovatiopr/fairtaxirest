package com.innovatio.fairtaxi.bean;

import com.innovatio.fairtaxi.validation.ValidFareEstimateRequest;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalTime;

@ValidFareEstimateRequest
public class FareEstimateRequest {

    private ServiceType serviceType;
    private LngLat origin;
    private LngLat dest;
    private double distanceInMiles;
    private Integer idleTimeSeconds;
    private Integer totalTripTimeSeconds;
    private int passengers = 1;
    private int baggage = 0;
    private boolean phoneCall;
    private LocalTime time = LocalTime.now();

    @NotNull
    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    @NotNull
    public LngLat getOrigin() {
        return origin;
    }

    public void setOrigin(LngLat origin) {
        this.origin = origin;
    }

    @NotNull
    public LngLat getDest() {
        return dest;
    }

    public void setDest(LngLat dest) {
        this.dest = dest;
    }

    public double getDistanceInMiles() {
        return distanceInMiles;
    }

    public void setDistanceInMiles(double distanceInMiles) {
        this.distanceInMiles = distanceInMiles;
    }

    public Integer getTotalTripTimeSeconds() {
        return totalTripTimeSeconds;
    }

    public void setTotalTripTimeSeconds(Integer totalTripTimeSeconds) {
        this.totalTripTimeSeconds = totalTripTimeSeconds;
    }

    public Integer getIdleTimeSeconds() {
        return idleTimeSeconds;
    }

    public void setIdleTimeSeconds(Integer idleTimeSeconds) {
        this.idleTimeSeconds = idleTimeSeconds;
    }

    @AssertTrue(message="Please include either totalTripTimeSeconds or idleTimeSeconds")
    public boolean validTrimTime() {
        return this.totalTripTimeSeconds != null || this.idleTimeSeconds != null;
    }

    @Min(1)
    public int getPassengers() {
        return passengers;
    }

    public void setPassengers(int passengers) {
        this.passengers = passengers;
    }

    @Min(0)
    public int getBaggage() {
        return baggage;
    }

    public void setBaggage(int baggage) {
        this.baggage = baggage;
    }

    public boolean isPhoneCall() {
        return phoneCall;
    }

    public void setPhoneCall(boolean phoneCall) {
        this.phoneCall = phoneCall;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }
}
