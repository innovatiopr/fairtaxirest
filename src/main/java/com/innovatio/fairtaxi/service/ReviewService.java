package com.innovatio.fairtaxi.service;

import com.innovatio.fairtaxi.bean.TripReview;
import com.innovatio.fairtaxi.entity.Review;
import com.innovatio.fairtaxi.repository.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by miguelmal on 10/6/18.
 */

@Service
public class ReviewService {

    @Autowired
    private ReviewRepository repository;

    public void addReview(TripReview tripReview) {

        Review review = new Review();
        review.setRating(tripReview.getRating());
        review.setIssue(tripReview.getIssue());
        review.setTag(tripReview.getTag());
        review.setEmail(tripReview.getEmail());
        review.setText(tripReview.getText());
        repository.save(review);
    }
}
