--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4 (Debian 10.4-2.pgdg90+1)
-- Dumped by pg_dump version 10.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: tarifas_por_zona; Type: TABLE; Schema: public; Owner: postgres
--
DROP TABLE IF EXISTS public.tarifas_por_zona;
CREATE TABLE public.tarifas_por_zona (
    origen text,
    destino text,
    tarifa numeric(16,2)
);


ALTER TABLE public.tarifas_por_zona OWNER TO postgres;

--
-- Name: taxi_zones; Type: TABLE; Schema: public; Owner: postgres
--
DROP TABLE IF EXISTS public.taxi_zones;
CREATE TABLE public.taxi_zones (
    fid integer NOT NULL,
    geom public.geometry(MultiPolygon,4326),
    zona character varying,
    abrev character varying,
    descripcion character varying(100)
);


ALTER TABLE public.taxi_zones OWNER TO postgres;

--
-- Name: taxi_zones taxi_zones_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.taxi_zones
    ADD CONSTRAINT taxi_zones_pkey PRIMARY KEY (fid);


--
-- Name: sidx_taxi_zones_geom; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sidx_taxi_zones_geom ON public.taxi_zones USING gist (geom);


--
-- PostgreSQL database dump complete
--

--
-- Name: sidx_taxi_zones_geom; Type: INDEX; Schema: public; Owner: postgres
--
DROP SEQUENCE IF EXISTS public.reviews_id_seq;
CREATE SEQUENCE public.reviews_id_seq;

ALTER TABLE public.reviews_id_seq OWNER TO postgres;

DROP TABLE IF EXISTS public.reviews;
CREATE TABLE public.reviews (
	id bigint DEFAULT nextval('reviews_id_seq'::regclass) NOT NULL,
	rating integer NOT NULL,
	issue text,
	"text" text,
	email text,
	tag text NOT NULL,
	PRIMARY KEY(id)
);

ALTER TABLE public.reviews OWNER TO postgres;
