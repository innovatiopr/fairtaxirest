package com.innovatio.fairtaxi.controller;

import com.innovatio.fairtaxi.bean.RecommendationRequest;
import com.innovatio.fairtaxi.bean.RecommendationResponse;
import com.innovatio.fairtaxi.bean.TripReview;
import com.innovatio.fairtaxi.service.RecommendationService;
import com.innovatio.fairtaxi.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by miguelmal on 10/6/18.
 */

@RestController
public class RecommendationController {

    @Autowired
    RecommendationService recommendationService;


    @PostMapping("/recommendations")
    RecommendationResponse recommendations(@RequestBody RecommendationRequest request) {
        return recommendationService.getRecomendations(request);
    }
}
