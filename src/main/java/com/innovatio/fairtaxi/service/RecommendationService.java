package com.innovatio.fairtaxi.service;

import com.innovatio.fairtaxi.bean.*;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.util.ArrayList;

/**
 * Created by miguelmal on 10/6/18.
 */
@Service
public class RecommendationService {

    public RecommendationResponse getRecomendations(RecommendationRequest request) {
        RecommendationResponse recomendations = new RecommendationResponse();
        recomendations.setRecommendations(new ArrayList<ViewPrInfo>());
        JSONArray jsonRecommendations;
        if (request.getType() == RecommendationType.RESTAURANT)
            jsonRecommendations = fetchViewPR(request.getLocation(), "Gastronomy");
        else if (request.getType() == RecommendationType.ATTRACTION)
            jsonRecommendations = fetchViewPR(request.getLocation(), "Attraction");
        else
            jsonRecommendations = new JSONArray();

        for (Object obj : jsonRecommendations) {
            JSONObject json = (JSONObject) obj;
            ViewPrInfo info = new ViewPrInfo();
            info.setName(jsonString(json, "name"));
            info.setType(jsonString(json, "types"));
            info.setCategory(jsonString(json, "categories"));
            info.setDescription(jsonString(json, "types"));

            info.setAddress(jsonString(json, "address"));
            info.setPhone(jsonString(json, "phone"));
            info.setWebsite(jsonString(json, "website"));

            LngLat lngLat = new LngLat();
            lngLat.setLat(jsonDouble(json, "latitude"));
            lngLat.setLng(jsonDouble(json, "longitude"));
            info.setLocation(lngLat);

            if (json.has("timetables"))
                info.setHours(processTimetables(json.getJSONArray("timetables")));
            if (json.has("media"))
                info.setImage(processMedia(json.getJSONArray("media")));

            recomendations.getRecommendations().add(info);
        }

        return recomendations;
    }

    private JSONArray fetchViewPR(LngLat location, String activity) {
        HttpClient httpclient = HttpClients.createDefault();
        try {
            URIBuilder builder = new URIBuilder("https://viewpr.azure-api.net/api/venue");

            builder.setParameter("latitude", Double.toString(location.getLat()));
            builder.setParameter("longitude", Double.toString(location.getLng()));
            builder.setParameter("radiusOfBuffer", "5");
            builder.setParameter("businessStatus", "1");
            builder.setParameter("activity", activity);

            URI uri = builder.build();
            HttpGet request = new HttpGet(uri);
            request.setHeader("Ocp-Apim-Subscription-Key", "41b717ca63254028a81d286c46832bc7");

            HttpResponse response = httpclient.execute(request);
            HttpEntity entity = response.getEntity();

            if (entity != null)
                return new JSONArray(
                        EntityUtils.toString(entity)
                );
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    private String processTimetables(JSONArray timetables) {
        String[] days = new String[7];
        days[0] = "Sun";
        days[1] = "Mon";
        days[2] = "Tue";
        days[3] = "Wed";
        days[4] = "Thu";
        days[5] = "Fri";
        days[6] = "Sat";
        String operation = "";
        for (Object obj : timetables) {
            JSONObject json = (JSONObject) obj;
            int day = jsonInt(json, "open_day");
            int open = jsonInt(json, "open_hour");
            int close = jsonInt(json, "close_hour");
            operation += days[day] + " " + open + "-" + close + ",";
        }
        if (operation.length() > 1)
            operation = operation.substring(0, operation.length()-1);
        return operation;
    }

    private String processMedia(JSONArray media) {
        return media.get(0).toString();
    }

    private String jsonString(JSONObject json, String key) {
        if (json.has(key))
            return json.getString(key);
        else
            return null;
    }

    private Double jsonDouble(JSONObject json, String key) {
        if (json.has(key))
            return json.getDouble(key);
        else
            return null;
    }
    private int jsonInt(JSONObject json, String key) {
        if (json.has(key))
            return json.getInt(key);
        else
            return 0;
    }

}
