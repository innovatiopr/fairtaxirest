package com.innovatio.fairtaxi.service;

import com.innovatio.fairtaxi.bean.*;
import com.innovatio.fairtaxi.dao.ZoneDao;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

@Service
public class FareEstimateService {

    private ZoneDao zoneDao;

    public FareEstimateService(ZoneDao zoneDao) {
        this.zoneDao = zoneDao;
    }

    public FareEstimateResponse estimateFare(FareEstimateRequest req) {

        FareEstimateResponse resp = new FareEstimateResponse();

        final List<Fee> fees = resp.getFees();

        if (ServiceType.TAXI == req.getServiceType()) {

            resp.setTripFeeType(TripFeeType.METER);

            final BigDecimal baseCharge = new BigDecimal("1.25");

            BigDecimal distanceFare = new BigDecimal(req.getDistanceInMiles() * 16).multiply(new BigDecimal("0.10"));
            BigDecimal idleTimeFare = new BigDecimal(getIdleTime(req) / 25).multiply(new BigDecimal("0.10"));
            BigDecimal totalMeterFare = distanceFare.add(idleTimeFare);

            final BigDecimal minFare = new BigDecimal("3.00");
            if(totalMeterFare.add(baseCharge).compareTo(minFare) > 0) {
                fees.add(new Fee("Initial charge", "Initial charge", baseCharge));
                fees.add(new Fee("Distance Meter Fare", "Meter Fare", distanceFare));
                fees.add(new Fee("Wait Time Meter Fare", "Wait Time Meter Fare", idleTimeFare));
            } else {
                fees.add(new Fee("Minimum Fare", "Minimum Fare", minFare));
            }



            if(req.getBaggage() > 0) {
                final BigDecimal baggageFee = new BigDecimal("1.00").multiply(new BigDecimal(req.getBaggage()));
                fees.add(new Fee("Baggage Fee X " + req.getBaggage(), "Baggage Fee X " + req.getBaggage(), baggageFee));
            }

            if(req.isPhoneCall()) {
                fees.add(new Fee("Taxi Company Phone Call Fee", "Taxi Company Phone Call Fee", new BigDecimal("1.00")));
            }

            if(req.getTime().isAfter(LocalTime.of(22, 0 ))
                    || req.getTime().isBefore(LocalTime.of(6, 0 ))) {
                fees.add(new Fee("Late Night Fee", "Late Night Fee", new BigDecimal("1.00")));
            }
            
        }

        if (ServiceType.TAXI_TURISTICO == req.getServiceType()) {

            final Fee zoneFee = zoneDao.getZoneFees(req.getOrigin(), req.getDest());
            if (zoneFee != null) {
                resp.setTripFeeType(TripFeeType.ZONE);
                fees.add(zoneFee);
            } else {

                resp.setTripFeeType(TripFeeType.METER);
                final BigDecimal baseCharge = new BigDecimal("1.75");

                BigDecimal distanceFare = new BigDecimal(req.getDistanceInMiles() * 19).multiply(new BigDecimal("0.10"));
                BigDecimal idleTimeFare = new BigDecimal(getIdleTime(req) / 25).multiply(new BigDecimal("0.10"));
                BigDecimal totalMeterFare = distanceFare.add(idleTimeFare);

                final BigDecimal minFare = new BigDecimal("4.00");
                if(totalMeterFare.add(baseCharge).compareTo(minFare) > 0) {
                    fees.add(new Fee("Initial charge", "Initial charge", baseCharge));
                    fees.add(new Fee("Distance Meter Fare", "Meter Fare", distanceFare));
                    fees.add(new Fee("Wait Time Meter Fare", "Wait Time Meter Fare", idleTimeFare));
                } else {
                    fees.add(new Fee("Minimum Fare", "Minimum Fare", minFare));
                }

            }

            if(zoneDao.isAirport(req.getOrigin())) {
                fees.add(new Fee("Airport Fee", "PR Tourism Co. Airport Fee", new BigDecimal("1.00")));
            }

            if(req.getBaggage() > 0) {
                final BigDecimal baggageFee = new BigDecimal("1.00").multiply(new BigDecimal(req.getBaggage()));
                fees.add(new Fee("Baggage Fee x " + req.getBaggage(), "Baggage Fee x " + req.getBaggage(), baggageFee));
            }

            if(req.getPassengers() > 5) {
                fees.add(new Fee("More than 5 Passengers Fee" + req.getPassengers(), "More than 5 Passengers Fee" + req.getPassengers(), new BigDecimal("2.00")));
            }

            if(req.isPhoneCall()) {
                fees.add(new Fee("Taxi Company Phone Call Fee", "Taxi Company Phone Call Fee", new BigDecimal("1.00")));
            }

            fees.add(new Fee("Gas surcharge", "Gas Surcharge", new BigDecimal("2.00")));

            if(req.getTime().isAfter(LocalTime.of(22, 0 ))
                    || req.getTime().isBefore(LocalTime.of(6, 0 ))) {
                fees.add(new Fee("Late Night Fee", "Late Night Fee", new BigDecimal("1.00")));
            }
        }

        BigDecimal total = resp.getFees()
                .stream()
                .map(Fee::getFee)
                .reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2, RoundingMode.HALF_UP);

        resp.setTotal(total);
        final BigDecimal tip = total.multiply(new BigDecimal("0.15").setScale(2, RoundingMode.HALF_UP));
        resp.setSuggestedTip(tip);
        resp.setTotalWithTip(total.add(tip));
        System.out.println("Rate: " + total);
        return resp;

    }

    public ServiceType getDefaultServiceType(LngLat lngLat) {
        String zone = zoneDao.getZone(lngLat);
        if(zone == null) {
            return ServiceType.TAXI;
        }

        if(Arrays.asList(
                "Zona 1",
                "Zona 2",
                "Zona 3",
                "Zona 4",
                "Zona 5",
                "Plaza Las Américas",
                "Mall of San Juan"
        ).contains(zone)) {
            return ServiceType.TAXI_TURISTICO;
        }

        return ServiceType.TAXI;
    }

    private int getIdleTime(FareEstimateRequest req) {

        if(req.getIdleTimeSeconds() != null) {
            return req.getIdleTimeSeconds();
        }

        final double tripTimeHours = req.getTotalTripTimeSeconds().doubleValue() / 360;
        double avgSpeedInMilesPerHour = req.getDistanceInMiles() / tripTimeHours;
        if(avgSpeedInMilesPerHour < 10) {
            return (int) (req.getTotalTripTimeSeconds() * 0.5);
        } else if (avgSpeedInMilesPerHour >= 10 && avgSpeedInMilesPerHour < 20) {
            return (int) (req.getTotalTripTimeSeconds() * 0.3);
        } else if (avgSpeedInMilesPerHour >= 20 && avgSpeedInMilesPerHour < 30) {
            return (int) (req.getTotalTripTimeSeconds() * 0.25);
        } else if (avgSpeedInMilesPerHour >= 30 && avgSpeedInMilesPerHour < 40) {
            return (int) (req.getTotalTripTimeSeconds() * 0.15);
        }
        return (int) (req.getTotalTripTimeSeconds() * 0.05);
    }



}
